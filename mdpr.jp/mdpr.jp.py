#!/usr/bin/env python3

"""Download the full sized images in a mdpr.jp gallery."""

import tempfile
import shutil
import os.path
import os
from urllib.parse import urlparse
from os.path import splitext, basename
from contextlib import closing

import requests
from bs4 import BeautifulSoup as bs4

s = requests.Session()

image_baseurl = "https://mdpr.jp"


def to_orig(url):
    """Rewrites the image urls found in the gallery to full sized versions.
    E.g. "https://mdpr.jp/photo/images/2017/01/07/w720_da1b201fdaa7a2e68f562dafdd824019435c4c4d994fea86.jpg"
    becomes "https://mdpr.jp/photo/images/2017/01/07/da1b201fdaa7a2e68f562dafdd824c4d994fea86.jpg".
    """

    o = urlparse(url)
    path = o.path.split('/')

    basename, fileext = splitext(path[-1])

    # Remove the resolution stuff
    basename = basename.split('_')[-1]

    # 07.01 23:56:26 <karaage> take the resized url, remove the w720_, set the
    # cursor before the dot of the file extension, press left 11 times and
    # backspace 8 times
    filename = '{}{}{}'.format(basename[:-19], basename[-11:], fileext)
    path[-1] = filename

    url = "{}://{}{}".format(o.scheme, o.netloc, '/'.join(path))

    return url


def parse_page(url):
    r = s.get(url)
    r.raise_for_status()

    soup = bs4(r.text, "html.parser")
    image_grid = soup.find('div', {'class': 'list-photo clearfix'})
    images = [to_orig('{}{}'.format(image_baseurl, a.get('src')))
              for a in image_grid.findAll('img')]

    return images


def download_file(url, filename, out_path):
    bytes_served = 0

    with tempfile.TemporaryDirectory() as temp_dir:
        temp_file = os.path.join(temp_dir, filename)
        with closing(s.get(url, stream=True)) as resp:
            with open(temp_file, 'wb') as file:
                for block in resp.iter_content(1024):
                    if not block:
                        break
                    file.write(block)
                    bytes_served += len(block)

        shutil.copyfile(temp_file, out_path)

    return bytes_served


def download_images(images, out_dir):
    for url in images:
        filename = basename(url)

        out_path = os.path.join(out_dir, filename)

        if os.path.exists(out_path):
            print('{} already downloaded, skipping.'.format(url))
            continue

        size = download_file(url, filename, out_path)

        print('Downloaded {} ({:.3g} MB) to {}'.format(url,
                                                       (size / 1024 / 1024),
                                                       out_path))


def main(args):
    if not os.path.isdir(args.dir):
        os.makedirs(args.dir, exist_ok=True)

    images = parse_page(args.url)
    download_images(images, args.dir)


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description=__doc__)

    parser.add_argument('url', type=str,
                        help='URL to a (picture in a) gallery.')
    parser.add_argument('dir', nargs='?', default=os.getcwd(),
                        help='Specifies the directory to save the images in.')

    args = parser.parse_args()
    main(args)
